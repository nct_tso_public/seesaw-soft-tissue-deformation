# SeeSaw - Soft Tissue Deformation Estimation
This project page relates to the work entitled "**SeeSaw: Learning Soft Tissue Deformation from Laparoscopy Videos with GNNs**"

**Authors:** Reuben Docea, Nithya Bhasker, Alexander C. Jenke, Fiona R. Kolbinger, Marius Distler, Carina Riediger, Jürgen Weitz, Stefanie Speidel and Micha Pfeiffer

A major challenge in soft tissue navigation systems is that structures often go out of sight of the camera, whether due to occlusion or being outside the field of view of the camera. The non-rigid nature of these structures complicates the task of navigating (localising the pose of the camera, and map-building) enormously. Keeping an up-to-date impression of these out-of-sight structures is of great importance to the usefullness of myriad methods beyond the beginning of surgery, such as registration in laparoscopic liver resections.

**SeeSaw is a Graph Neural Network (GNN) based method for abdominal surgery, which uses an initial 3D scene state, represented as a graph, together with knowledge of how the part of the 3D scene changed to a later stage, in order to predict the complete state of the (sparsified) 3D scene.** 

This is learned from many such cases prepared from stereo laparoscopy videos, leveraging depth estimation and feature detection and matching. From depth maps and delaunay triangulation of keypoints extracted through feature detection, a surface mesh of the surgical scene captured in a stereo image pair of a laparoscope is formed. By matching those keypoints across different timepoints, displacements for the matched keypoints can be obtained. Using this information, the task at hand is to predict, for all of the keypoints (or graph nodes) which are unmatched (~90%), what they're displacement was. Performing this task would enable predicting the positions of soft tissue which is occluded from view or goes off-screen. The learning formulation which we use to achieve this task is similar to inpainting, and involves partitioning the matched nodes, and therefore the known displacements into a visible and hidden set (green and blue, respectively, in the figure below - red and magenta are unmatched and predicted, respectively).

<img src="files/SeeSaw_Diagram.png" alt="learning_setup" width="1200">

The model is trained by making predictions of the displacements for all of the nodes based on the visible set of displacements, backpropagating errors on the hidden set. 

<div style="text-align: center;">
    <img src="files/learning_setup.png" alt="learning_setup" width="300" height="300">
</div>

Below are two examples illustrating predictions from our model.
(N.B., the displacements concerned are what remain after a rough alignment using SVD).

![A_full.gif](files%2FA_full.gif)

![B_full.gif](files%2FB_full.gif)

# License
This work released under an MIT license.

When referencing this work in academic publications please use the following citation:

    @article{docea2024seesaw,
    title={SeeSaw: Learning Soft Tissue Deformation from Laparoscopy Videos with GNNs},
    author={Docea, Reuben and Xu, Jinjing and Ling, Wei and Jenke, Alexander C and Kolbinger, Fiona R and Distler, Marius and Riediger, Carina and Weitz, J{\"u}rgen and Speidel, Stefanie and Pfeiffer, Micha},
    journal={IEEE Transactions on Biomedical Engineering},
    year={2024},
    publisher={IEEE}
    }

# Run Examples
## Setup
This example uses python 3.11, Pytorch 2.3 and cuda 12.1. But these requirements are not strict and earlier versions 
should work. Install from the requirements file as follows:

    pip install -r requirements.txt

Install the following dependences (as follows for Pytorch 2.3 and Cuda 12.1):

    pip install  dgl -f https://data.dgl.ai/wheels/torch-2.3/cu121/repo.html

(optional):

    pip install pyg_lib torch_scatter torch_sparse torch_cluster torch_spline_conv -f https://data.pyg.org/whl/torch-2.3.0+cu121.html

## Run
You should then be able to run the model of the given samples:

    python ./demo.py --config config.json

To visualise the outputs, you can add the '--visualise' flag:

    python ./demo.py --config config.json --visualise

# Acknowledgement
The authors gratefully acknowledge funding for this research by the State of Saxony via Sachsische Aufbaubank (SAB) in 
the scope of the ARAILIS project (100400076). This measure is co-financed with tax funds on the basis of the budget 
passed by the Saxon state parliament. This project has received funding from the European Union’s Horizon Europe 
research and innovation programme under grant agreement No 101092646. In addition, this work was supported by the 
German Research Foundation (DFG, Deutsche Forschungsgemeinschaft) as part of Germany’s Excellence Strategy - EXC 2050/1 - 
Project ID 390696704 - Cluster of Excellence “Centre for Tactile Internet with Human-in-the-Loop” (CeTI) as well as by - 
the German Federal Ministry of Health (BMG) within the SurgOmics-project (grant number BMG 2520DAT82)