import numpy as np
import open3d as o3d
def points(points, base_colour=[1, 1, 0], colours=None):
    """
    Return points object for viewing.
    base_colour: colour of mesh, e.g. base_colour=[1,1,0]
    """

    points_set = o3d.geometry.PointCloud()
    points_set.points = o3d.utility.Vector3dVector(points)

    colour = colours if colours is not None else np.full(points.shape, base_colour)
    points_set.colors = o3d.utility.Vector3dVector(colour)

    return [points_set]


def single_mesh(points, edges, **kwargs):
    """
    View a single point cloud mesh.
    points: numpy array of shape (n, 3)
    edges: numpy array of shape (2, n)
    base_colour: colour of mesh, e.g. base_colour=[1, 1, 0]
    colours: colours of lines
    """

    base_colour = kwargs.get("base_colour", [0, 0, 0])
    edge_mask = kwargs.get("edge_mask", np.ones(edges.shape[1], dtype=bool))
    colours = kwargs.get("colours")

    lines = [[edges[0, i], edges[1, i]] for i in range(edges.shape[1]) if edges[0, i] != edges[1, i]]

    if colours is not None:
        edge_colours = [list(colours[i]) for i in range(colours.shape[0])]
    else:
        edge_colours = [base_colour] * len(lines)

    masked_lines = [line for i, line in enumerate(lines) if edge_mask[i]]
    masked_edge_colours = [edge_colours[i] for i in range(len(lines)) if edge_mask[i]]

    line_set = o3d.geometry.LineSet()
    line_set.points = o3d.utility.Vector3dVector(points)
    line_set.lines = o3d.utility.Vector2iVector(masked_lines)
    line_set.colors = o3d.utility.Vector3dVector(masked_edge_colours)

    return [line_set]

def rotate_camera_around_y(camera_parameters, angle_in_degrees):
    """
    Rotates the camera around the Y axis by the given angle in degrees.
    """
    R = camera_parameters.extrinsic[:3, :3].copy()
    t = camera_parameters.extrinsic[:3, 3].copy()
    t[-1] = t[-1] * 0.1

    # Create a rotation matrix around the Y axis
    angle = np.radians(angle_in_degrees)
    rotation_matrix = np.array([
        [np.cos(angle), 0, np.sin(angle)],
        [0, 1, 0],
        [-np.sin(angle), 0, np.cos(angle)]
    ])

    # Apply rotation
    R_rotated = rotation_matrix @ R

    # Create a new extrinsic matrix
    extrinsic_rotated = np.eye(4)
    extrinsic_rotated[:3, :3] = R_rotated
    extrinsic_rotated[:3, 3] = t

    # Create new camera parameters object
    new_camera_parameters = o3d.camera.PinholeCameraParameters()
    new_camera_parameters.intrinsic = camera_parameters.intrinsic
    new_camera_parameters.extrinsic = extrinsic_rotated

    return new_camera_parameters

def view(toDraw, height=1080, width=1920, fullscreen=False):
    vis = o3d.visualization.Visualizer()
    vis.create_window(width=width, height=height)
    if fullscreen:
        vis.set_full_screen(True)

    for item in toDraw:
        vis.add_geometry(item)

    print("Visualising...")

    ctr = vis.get_view_control()
    parameters = ctr.convert_to_pinhole_camera_parameters()
    #parameters = rotate_camera_around_y(parameters, 180)
    ctr.convert_from_pinhole_camera_parameters(parameters)

    vis.run()
    vis.destroy_window()
def cloudFromRGBDisparity(image, disparity, camParams=[534.195, 534.095, 248.45, 181.37, 2.110638907462192]):
    fx, fy, cx, cy, bf = camParams  # Unpack camera parameters
    depth = (bf / disparity) * 1000  # Calculate depth and convert to millimeters

    rgb = o3d.geometry.Image(image.astype(np.uint8))
    depth_image = o3d.geometry.Image(depth.astype(np.float32))
    rgbd = o3d.geometry.RGBDImage.create_from_color_and_depth(rgb, depth_image, convert_rgb_to_intensity=False)

    intrinsic = o3d.camera.PinholeCameraIntrinsic(
        width=image.shape[1],
        height=image.shape[0],
        fx=fx,
        fy=fy,
        cx=cx,
        cy=cy
    )

    return o3d.geometry.PointCloud.create_from_rgbd_image(rgbd, intrinsic)
