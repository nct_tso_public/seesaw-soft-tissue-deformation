import torch
from network_models.common_modules import SkipMLP, MsgMLP

class SeeSaw(torch.nn.Module):
    def __init__(self, n_msg_pass_hops, **kwargs):
        super(SeeSaw, self).__init__()
        self.n_msg_pass_hops = n_msg_pass_hops
        self.device = kwargs.get("device")
        self.n_hidden = kwargs.get("n_hidden", 128)
        self.aggr = kwargs.get("aggr", 'max')
        self.n_hidden_layers = kwargs.get("n_hidden_layers", 1)
        self.activation = kwargs.get("activation", torch.nn.Tanh())
        self.use_dense = kwargs.get("use_dense")
        self.args = kwargs.get("args")

        self.rolloutNet = RolloutModule(
            n_msg_pass_hops=self.n_msg_pass_hops,
            args=self.args,
            activation=self.activation,
            device=self.device,
            n_hidden=self.n_hidden,
            aggr=self.aggr,
            n_hidden_layers=self.n_hidden_layers,
            use_dense=self.use_dense
        )

    def forward(self, data):
        # Initialize displacement tensor with zeros, except for observed nodes
        _displacements = torch.zeros_like(data.displacements).to(self.device)
        _displacements[data.rollout_nodes[:, 0], :] = data.displacements[data.rollout_nodes[:, 0], :]

        if self.args.use_all_matches:
            # Assign all matched nodes to be visible nodes
            data.visible_nodes = data.matched_nodes
            data.rollout_nodes[:, 0] = data.matched_nodes

        if not self.args.extra_edges:
            for i in range(data.n_rollouts):
                # Determine predicted nodes for the current rollout step
                predicted_nodes = data.rollout_nodes[:, i + 1] ^ data.rollout_nodes[:, i]

                # Compute updates from the network using the current state of displacements
                _displacements_updates = self.rolloutNet(data, _displacements, i)

                # Update only the nodes which were predicted in this iteration
                _displacements[predicted_nodes, :] = _displacements_updates[predicted_nodes, :]
        else:
            # Predict all nodes at once if extra edges are used
            predicted_nodes = data.rollout_nodes[:, -1] ^ data.rollout_nodes[:, 0]

            # Compute updates from the network using the current state of displacements
            _displacements_updates = self.rolloutNet(data, _displacements, 0)

            # Update only the nodes which were predicted
            _displacements[predicted_nodes, :] = _displacements_updates[predicted_nodes, :]

        return _displacements  # _displacements tensor now contains 'updated' node displacements


class RolloutModule(torch.nn.Module):
    def __init__(self, n_msg_pass_hops, **kwargs):
        super(RolloutModule, self).__init__()
        self.n_msg_pass_hops = n_msg_pass_hops
        self.device = kwargs.get("device")
        self.n_hidden = kwargs.get("n_hidden", 128)
        self.aggr = kwargs.get("aggr", 'mean')
        self.n_hidden_layers = kwargs.get("n_hidden_layers", 1)
        self.activation = kwargs.get("activation", torch.nn.Tanh())
        self.args = kwargs.get("args")

        print("Setting model up for real data")

        nodeEncoderDim = 4 + (3 if self.args.use_unaligned else 0)
        self.nodeEncoder = SkipMLP(nodeEncoderDim, self.n_hidden, 'encoder',
                                   activation=self.activation, n_hidden=self.n_hidden, n_hidden_layers=self.n_hidden_layers)

        self.edgeEncoder = SkipMLP(4, self.n_hidden, 'encoder',
                                   activation=self.activation, n_hidden=self.n_hidden, n_hidden_layers=self.n_hidden_layers)

        self.msgPass = torch.nn.ModuleList([
            MsgMLP(self.n_hidden, self.n_hidden, aggr=self.aggr, activation=self.activation,
                   n_hidden=self.n_hidden, useAttr=True, n_hidden_layers=self.n_hidden_layers)
            for _ in range(self.n_msg_pass_hops)
        ])

        self.attrUpdate = torch.nn.ModuleList([
            SkipMLP(self.n_hidden * 3, self.n_hidden, 'encoder', activation=self.activation,
                    n_hidden=self.n_hidden, n_hidden_layers=self.n_hidden_layers)
            for _ in range(self.n_msg_pass_hops)
        ])

        self.nodeOutputDecoder = SkipMLP(self.n_hidden, 3, 'decoder',
                                         activation=self.activation, n_hidden_layers=self.n_hidden_layers)

    def forward(self, data, _displacements, step_no):
        edge_lengths = data.edge_lengths if not self.args.extra_edges else (data.initial_positions[data.edge_index[0]] - data.initial_positions[data.edge_index[1]])
        visible_nodes = data.visible_nodes
        edge_index = data.edge_index
        data.initial_positions = data.initial_positions.to(self.device)

        node_status = torch.zeros_like(data.visible_nodes, device=self.device)
        node_status[data.rollout_nodes[:, step_no]] = 1

        node_attr = torch.cat((node_status.view(-1, 1), _displacements, data.node_type), dim=1)

        if self.args.use_unaligned:
            _displacements_unaligned = torch.zeros_like(data.displacements_unaligned, device=self.device)
            _displacements_unaligned[visible_nodes] = data.displacements_unaligned[visible_nodes]
            node_attr = torch.cat((node_attr, _displacements_unaligned), dim=1)

        node_attr = self.nodeEncoder(node_attr)
        edge_norm = edge_lengths.norm(dim=1, keepdim=True)
        edge_attr = torch.cat((edge_lengths, edge_norm), dim=1)
        edge_attr = self.edgeEncoder(edge_attr)

        for i in range(self.n_msg_pass_hops):
            edge_attr = self.attrUpdate[i](
                torch.cat((node_attr[edge_index[0]], node_attr[edge_index[1]], edge_attr), dim=-1))
            node_attr = self.activation(self.msgPass[i](node_attr, edge_index, edge_attr))

        return self.nodeOutputDecoder(node_attr)
