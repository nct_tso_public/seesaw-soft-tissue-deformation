# -*- coding: utf-8 -*-
import torch
from torch_geometric.nn import MessagePassing
from dgl.geometry import farthest_point_sampler
import numpy as np
class SkipMLP(torch.nn.Module):
    def __init__(self,in_channels,out_channels,type=None,**kwargs):
        super(SkipMLP, self).__init__()
        self.type = type
        self.in_channels = in_channels
        self.out_channels = out_channels

        self.n_hidden = kwargs.get("n_hidden")
        if self.n_hidden is None:
            self.n_hidden = 128

        self.n_hidden_layers = kwargs.get("n_hidden_layers")
        if self.n_hidden_layers is None or self.n_hidden_layers < 0:
            self.n_hidden_layers = 2

        self.activation = kwargs.get("activation")
        if self.activation is None:
            self.activation = torch.nn.Tanh()

        self.linearList = torch.nn.ModuleList()
        self.linearList.append(torch.nn.Linear(self.in_channels, self.n_hidden))
        for i in range(self.n_hidden_layers):
            self.linearList.append(torch.nn.Linear(self.n_hidden, self.n_hidden))
        self.linearList.append(torch.nn.Linear(self.n_hidden, self.out_channels))

    def forward(self, x):
        # inspiration from : https://github.com/pytorch/vision/blob/a9a8220e0bcb4ce66a733f8c03a1c2f6c68d22cb/torchvision/models/resnet.py#L56-L72
        if (self.type=='normal' and x.shape[0]==self.n_hidden) or self.type=='msgPass':
            #identity = x[:,:self.n_hidden].clone()
            identity = x.clone()
        out = self.linearList[0](x)
        if (self.type=='normal' and x.shape[0]!=self.n_hidden) or self.type=='encoder':
            identity = out.clone()
        out = self.activation(out)

        for i in range(self.n_hidden_layers):
            out = self.activation(self.linearList[1+i](out))

        out = self.linearList[-1](out)

        if self.type=='normal' or self.type=='encoder':
            out += identity
            out = self.activation(out)
        #if self.type=='encoder':
        #    out = self.act(out)
        if self.type=='msgPass':
            out += identity
        return out


class MsgMLP(MessagePassing):
    def __init__(self, in_channels, out_channels, **kwargs):
        aggr = kwargs.get("aggr")
        if aggr is None:
            aggr = 'mean'
        super(MsgMLP, self).__init__(aggr=aggr, flow='source_to_target')

        self.n_hidden = kwargs.get("n_hidden")
        if self.n_hidden is None:
            self.n_hidden = 128

        self.n_hidden_layers = kwargs.get("n_hidden_layers")
        if self.n_hidden_layers is None or self.n_hidden_layers < 0:
            self.n_hidden_layers = 2

        self.useAttr = kwargs.get("useAttr")

        self.activation = kwargs.get("activation")
        if self.activation is None:
            self.activation = torch.nn.Tanh()

        self.mlp = SkipMLP(in_channels,out_channels,'msgPass', activation=self.activation, n_hidden=self.n_hidden, n_hidden_layers=self.n_hidden_layers)

    def forward(self, x, edge_index, edge_attr=None):
        return self.propagate(edge_index, x=x, edge_attr=edge_attr, size=None)

    def message(self, x_j, edge_attr):
        return edge_attr

def add_fps_edges(data, n_nodes=49):
    edge_index = get_fps_edges(data.initial_positions, n_nodes=n_nodes).to(data.initial_positions.device)
    data.edge_index = torch.cat((data.edge_index, edge_index), 1)
    return data
def get_fps_edges(pos, n_nodes=49):
    pos = pos.cpu()

    sampled_idx = farthest_point_sampler(pos.view((1, pos.shape[0], 3)), n_nodes)[0]

    combinations = np.array(np.meshgrid(np.arange(n_nodes), np.arange(n_nodes))).T.reshape(-1, 2)
    combinations = combinations[combinations[:, 0] != combinations[:, 1]]

    # Create edge index from all combinations of sampled_idx
    edge_index = torch.zeros((2, n_nodes * (n_nodes - 1)), dtype=torch.long)
    edge_index[0] = sampled_idx[combinations[:, 0]]
    edge_index[1] = sampled_idx[combinations[:, 1]]

    return edge_index