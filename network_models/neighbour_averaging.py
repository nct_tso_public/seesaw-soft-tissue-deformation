import numpy as np
import torch

class NeighbourAveraging(torch.nn.Module):
    def __init__(self, **kwargs):
        super(NeighbourAveraging, self).__init__()

        self.args = kwargs.get("args")
        self.device = kwargs.get("device")

    def forward(self, data):
        data = data.to("cpu")
        displacements = torch.zeros(data.initial_positions.shape).to("cpu")

        displacements[data.rollout_nodes[:, 0], :] = (data.current_positions - data.initial_positions)[data.rollout_nodes[:, 0],:]
        displacements = displacements.numpy()

        for i in range(data.rollout_nodes.shape[1] - 1):
            old_nodes = data.rollout_nodes[:, i]
            new_nodes = data.rollout_nodes[:, i + 1] ^ data.rollout_nodes[:, i]
            new_nodes_idx = list(np.where(new_nodes)[0])
            tmp_adj_mat = np.copy(data.adj_mat)
            if len(tmp_adj_mat.shape) == 3:
                tmp_adj_mat = tmp_adj_mat[0]

            tmp_adj_mat[data.rollout_nodes[:, i + 1] ^ True, :] = False
            tmp_adj_mat[:, data.rollout_nodes[:, i + 1] ^ True] = False
            tmp_adj_mat = tmp_adj_mat[new_nodes_idx, :]
            tmp_adj_mat[:, new_nodes_idx] = False
            for k in range(tmp_adj_mat.shape[0]):
                displacements[new_nodes_idx[k], :] = np.mean(displacements[tmp_adj_mat[k, :]], 0)

        data = data.to(self.device)
        displacements = torch.tensor(displacements).to(self.device)

        return displacements # _displacements tensor now contains 'updated' node displacements