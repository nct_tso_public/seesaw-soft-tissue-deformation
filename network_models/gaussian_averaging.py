import numpy as np
import torch

class GaussianWeightedAveraging(torch.nn.Module):
    def __init__(self, sd_distance=0.0015, **kwargs):
        super(GaussianWeightedAveraging, self).__init__()

        self.args = kwargs.get("args")
        self.device = kwargs.get("device")
        self.sd_distance = sd_distance
        self.iterative = kwargs.get("iterative")
        if self.iterative == None:
            self.iterative = False

    def forward(self, data):
        data = data.to("cpu")
        displacements = np.zeros(data.initial_positions.shape)

        initial_positions = data.initial_positions.numpy()
        current_positions = data.current_positions.numpy()

        displacements[data.rollout_nodes[:, 0], :] = (current_positions - initial_positions)[data.rollout_nodes[:, 0],:]

        for i in range(data.rollout_nodes.shape[1]-1) if self.iterative else range(1):

            # Create grid of displacements for each node
            grid_displacements = np.repeat(displacements.reshape(1,displacements.shape[0],displacements.shape[1]),
                                           displacements.shape[0],
                                           axis=0)

            # Compute distance between each node position compared to all other positions
            distances = np.linalg.norm(initial_positions[:, np.newaxis, :] - initial_positions[np.newaxis, :, :], axis=2)

            # Compute influence of each node based on gaussian kernel
            influence = np.exp(-distances**2 / (2 * self.sd_distance**2))

            # set influence of nodes not in initial rollout step to 0
            influence[:, data.rollout_nodes[:, i] ^ True] = 0

            # Relax influence if node is not in current rollout step
            sum_influence = np.sum(influence, axis=1).reshape(influence.shape[0], 1)
            if ((sum_influence<0.0000001)[data.rollout_nodes[:,i]^data.rollout_nodes[:,i+1]]).sum()>0:
                tmp_sd_distance = self.sd_distance
                while ((sum_influence<0.0000001)[data.rollout_nodes[:,i]^data.rollout_nodes[:,i+1]]).sum() > 0:
                    tmp_sd_distance *= 2

                    tmp_influence = np.exp(-distances**2 / (2 * tmp_sd_distance**2))
                    tmp_influence[:, data.rollout_nodes[:, i] ^ True] = 0

                    remaining_nodes = (sum_influence<0.0000001)
                    remaining_nodes[(data.rollout_nodes[:,i]^data.rollout_nodes[:,i+1]) ^ True] = False

                    influence[remaining_nodes[:,0], :] = tmp_influence[remaining_nodes[:,0], :]
                    sum_influence = np.sum(influence, axis=1).reshape(influence.shape[0], 1)

            tmp_displacements = np.sum(np.multiply(influence.reshape(influence.shape[0], influence.shape[1], 1), grid_displacements) # Weights displacements by influence
                                       , axis=1)
            tmp_displacements = tmp_displacements / np.sum(influence, axis=1).reshape(influence.shape[0], 1)

            if self.iterative:
                displacements[data.rollout_nodes[:, i] ^ data.rollout_nodes[:, i+1], :] = tmp_displacements[data.rollout_nodes[:, i] ^ data.rollout_nodes[:, i+1], :]
            else:
                displacements[data.rollout_nodes[:, 0] ^ data.rollout_nodes[:, -1], :] = tmp_displacements[data.rollout_nodes[:, 0] ^ data.rollout_nodes[:, -1], :]

        data = data.to(self.device)
        displacements = torch.tensor(displacements).to(self.device)

        return displacements