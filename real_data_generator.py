# -*- coding: utf-8 -*-
import torch
from torch_geometric.data import Data
class MultiData(Data):
    def __init__(self, **kwargs):
        super(MultiData, self).__init__()

        self.initial_positions = kwargs.get("initial_positions")
        self.current_positions = kwargs.get("current_positions")
        self.current_positions_unaligned = kwargs.get("current_positions_unaligned")
        self.displacements = kwargs.get("displacements")
        self.displacements_unaligned = kwargs.get("displacements_unaligned")
        self.baseline_displacements = kwargs.get("baseline_displacements")
        self.edge_lengths = kwargs.get("edge_lengths")
        self.visible_nodes = kwargs.get("visible_nodes")
        self.visible_edges = kwargs.get("visible_edges")
        self.edge_index = kwargs.get("edge_index")
        self.node_type = kwargs.get("node_type")
        self.rollout_nodes = kwargs.get("rollout_nodes")
        self.rollout_edges = kwargs.get("rollout_edges")
        self.n_rollouts = kwargs.get("n_rollouts")
        self.confidence = kwargs.get("confidence")
        self.matched_nodes = kwargs.get("matched_nodes")
        self.matched_tissue_nodes = kwargs.get("matched_tissue_nodes")
        self.learnable_nodes = kwargs.get("learnable_nodes")

        self.keypoints = kwargs.get("keypoints")
        self.keypoints_def = kwargs.get("keypoints_def")
        self.disparity = kwargs.get("disparity")
        self.disparity_def = kwargs.get("disparity_def")
        self.image = kwargs.get("image")
        self.image_def = kwargs.get("image_def")
        self.seg = kwargs.get("seg")
        self.seg_def = kwargs.get("seg_def")
        self.seg_init = kwargs.get("seg_init")
        self.matches = kwargs.get("matches")
        self.confidence = kwargs.get("confidence")
        self.descriptors = kwargs.get("descriptors")
        self.xyz = kwargs.get("xyz")
        self.xyz_def = kwargs.get("xyz_def")

        self.depth_mask_intersections = kwargs.get("depth_mask_intersections")
        self.cloud_points = kwargs.get("cloud_points")
        self.cloud_colors = kwargs.get("cloud_colors")
        self.sample_path = kwargs.get("sample_path")
        self.svd_translation = kwargs.get("svd_translation")
        self.svd_rotation = kwargs.get("svd_rotation")
        self.adj_mat = kwargs.get("adj_mat")
        self.displacement_prime = kwargs.get("displacement_prime")
        self.tissue_nodes = kwargs.get("tissue_nodes")
        self.occlusion_mask = kwargs.get("occlusion_mask")

        self.verified_match_nodes = kwargs.get("verified_match_nodes")
        self.verified_kp_idx = kwargs.get("verified_kp_idx")

    def __inc__(self, key, value, *args, **kwargs):
        if 'edge_index' in key:
            return self.rollout_nodes.size(0)
        elif 'keypoints' in key:
            return self.image.size()[2] # height
        else:
            return 0

    def __cat_dim__(self, key, value, *args, **kwargs):
        if 'initial_positions' in key or 'current_positions' in key or 'displacements' in key or 'baseline_displacements' in key or 'baseline_displacements' in key or 'edge_lengths' in key or 'visible_nodes' in key or 'visible_edges' in key or 'node_type' in key or 'rollout_nodes' in key or 'rollout_edges' in key or 'confidence' in key or 'matched_nodes' in key or 'matched_tissue_nodes' in key or 'learnable_nodes' in key or 'descriptors' in key or 'depth_mask_intersections' in key:
            return 0
        if 'edge_index' in key:
            return 1
        if 'image' in key:
            return 0
        if 'keypoints' in key:
            return 0

if __name__ == "__main__":
    data = MultiData()
