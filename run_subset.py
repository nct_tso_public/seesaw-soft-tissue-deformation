# -*- coding: utf-8 -*-
import os
import torch
import numpy as np
import open3d as o3d
from scipy.stats import norm
from tqdm import tqdm
import time
from tabulate import tabulate
from utils.visualisation.Visualisation import  *
import cv2
def run_subset(**kwargs):
    args = kwargs.get("args")
    device = kwargs.get("device")
    models = kwargs.get("models")
    data_list = kwargs.get("data_list")
    model_key_list = kwargs.get("model_key_list")

    results = []

    for i in tqdm(range(len(data_list)), leave=False, desc="Samples"):
        data = data_list[i].clone()
        data = data.to(device)

        time_model = time.time()
        # Assume the first model is used for timing and displacement calculations
        first_model_key = model_key_list[0]
        first_model = models[first_model_key]
        predicted_displacements = first_model(data)
        time_model = time.time() - time_model

        predicted_nodes = data.rollout_nodes[:, -1] ^ data.rollout_nodes[:, 0]

        # Remove nodes with a displacement magnitude greater than 3 std:
        outlier_nodes = torch.zeros(data.visible_nodes.size(), dtype=torch.bool).to(device)
        tmp_displacements = data.displacements[data.matched_tissue_nodes, :]
        displacement_magnitudes = np.linalg.norm(tmp_displacements.clone().cpu().numpy(), axis=1)

        mu, std = norm.fit(displacement_magnitudes)
        outlier_idx = np.asarray(np.where(displacement_magnitudes > mu + 3 * std))[0]

        tmp = torch.zeros(outlier_nodes[data.matched_tissue_nodes].size(), dtype=torch.bool).to(device)
        tmp[outlier_idx] = True
        outlier_nodes[data.matched_tissue_nodes] = tmp

        predicted_nodes[outlier_nodes] = False

        predicted_nodes = predicted_nodes * data.learnable_nodes  # Exclude non-learnable nodes

        unaligned_predicted_node_displacements = data.displacements_unaligned[predicted_nodes]
        aligned_predicted_node_displacements = data.displacements[predicted_nodes]

        unaligned_euclidean_distance_to_target = torch.norm(unaligned_predicted_node_displacements)
        aligned_euclidean_distance_to_target = torch.norm(aligned_predicted_node_displacements)

        row_result = [
            i,
            "{:.4f}s".format(time_model),
            "{:.2f}mm".format(unaligned_euclidean_distance_to_target * 1000),
            "{:.2f}mm".format(aligned_euclidean_distance_to_target * 1000)
        ]

        for model_key in model_key_list:
            model = models[model_key]
            predicted_displacements = model(data)
            predicted_node_displacements = predicted_displacements[predicted_nodes]

            predicted_euclidean_distance_to_target = torch.norm(aligned_predicted_node_displacements -
                                                                predicted_node_displacements)

            row_result.append("{:.2f}mm".format(predicted_euclidean_distance_to_target * 1000))

            if args.visualise:
                orig_data = torch.load(os.path.join('samples_upstream', data.sample_path[0]))

                data.image = orig_data['image'][0]
                data.disparity = orig_data['disparity']

                tmp_image = data.image.cpu().numpy().astype(np.uint8)
                tmp_disparity = data['disparity'][0].cpu().numpy()

                clouds = [cloudFromRGBDisparity(cv2.cvtColor(tmp_image, cv2.COLOR_BGR2RGB), tmp_disparity)]
                clouds[0].points = o3d.utility.Vector3dVector(np.asarray(clouds[0].points) + [0, 0, 0.0005])

                initial_positions = data.initial_positions.cpu().numpy()
                displacements = data.displacements.cpu().numpy()
                unaligned_displacements = data.displacements_unaligned.cpu().numpy()
                rollout_nodes = data.rollout_nodes.cpu().numpy()[:, 0]
                learnable_nodes = data.learnable_nodes.cpu().numpy()

                all_positions = np.vstack((initial_positions, initial_positions + displacements))
                all_predicted_positions = np.vstack(
                    (initial_positions, initial_positions + predicted_displacements.cpu().numpy()))
                all_unaligned_positions = np.vstack((initial_positions, initial_positions + unaligned_displacements))

                visible_edges = np.vstack(
                    (np.where(rollout_nodes)[0], np.where(rollout_nodes)[0] + initial_positions.shape[0]))

                all_nodes = np.ones_like(learnable_nodes, dtype=bool)
                all_edges = np.vstack((np.where(all_nodes)[0], np.where(all_nodes)[0] + initial_positions.shape[0]))

                toDraw = (
                        single_mesh(all_predicted_positions, all_edges, base_colour=[1, 0, 1]) +
                        single_mesh(all_positions, visible_edges, base_colour=[0, 1, 0]) +
                        # single_mesh(all_unaligned_positions, visible_edges, base_colour=[0, 1, 1]) +
                        points(initial_positions[rollout_nodes], base_colour=[0, 1, 0]) +
                        points(initial_positions[(~rollout_nodes) & (~learnable_nodes)], base_colour=[1, 0, 0]) +
                        points(initial_positions[learnable_nodes], base_colour=[0, 1, 0]) +
                        clouds +
                        single_mesh(initial_positions, data.edge_index.cpu().numpy(), base_colour=[0, 0, 0])
                )

                print("viewing...")
                view(toDraw, fullscreen=True)

        results.append(row_result)

    # Dynamically create headers
    headers = ["Sample", "Time for Prediction", "Primary Displacement", "Residual Displacement"]
    headers.extend([f"Error {model_key}" for model_key in model_key_list])

    # Print results in tabular format
    print(tabulate(results, headers=headers, tablefmt="grid"))
