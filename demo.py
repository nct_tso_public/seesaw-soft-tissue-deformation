# -*- coding: utf-8 -*-
import sys
import os
import json
import numpy as np
import torch
import argparse
from network_models.seesaw_model import SeeSaw
from network_models.neighbour_averaging import NeighbourAveraging
from network_models.gaussian_averaging import GaussianWeightedAveraging
from run_subset import run_subset

def load_config(config_path):
    with open(config_path, 'r') as f:
        config = json.load(f)
    return config

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('--config', type=str, required=True, help='Path to the configuration file')

    # Optional arguments
    parser.add_argument('--visualise', action='store_true')
    parser.add_argument('--cpu', action='store_true')

    # Baseline arguments
    parser.add_argument('--no_model', action='store_true', help='Do not use model')
    parser.add_argument('--use_neighbour_baseline', action='store_true')
    parser.add_argument('--use_gaussian_baseline', action='store_true')
    parser.add_argument('--gaussian_sd', type=float, default=0.00142, help='Standard deviation of distance for influence in gaussian kernel method')
    parser.add_argument('--use_all_matches', action='store_true', help='Use all matches for computing displacements')

    args = parser.parse_args()

    # Load configuration from the file
    config = load_config(args.config)

    # Update args with the configuration values
    for key, value in config.items():
        setattr(args, key, value)

    model_key_list = []
    if not getattr(args, 'no_model', False):
        model_key_list.append("gnn")
    if args.use_neighbour_baseline:
        model_key_list.append("neighbour_avg")
    if args.use_gaussian_baseline:
        model_key_list.append("gaussian_avg")

    np.random.seed(0)
    torch.manual_seed(0)

    device = torch.device('cpu' if args.cpu else 'cuda')
    print("DEVICE:", device)

    activation = getattr(torch.nn, args.activation)()

    if args.model_path:
        print("model_path:", args.model_path)

    model = SeeSaw(args.n_msg_pass_hops, device=device, n_hidden=args.n_hidden, aggr=args.aggr,
                   n_hidden_layers=args.n_hidden_layers, activation=activation, args=args)
    if args.model_path:
        model.load_state_dict(torch.load(args.model_path))
    model = model.to(device)

    neighbour_averager = NeighbourAveraging(args=args, device=device).to(device)
    gaussian_averager = GaussianWeightedAveraging(args.gaussian_sd, args=args, device=device, iterative=True).to(device)

    models = {'neighbour_avg': neighbour_averager, 'gaussian_avg': gaussian_averager, 'gnn': model}

    data_list = [torch.load(os.path.join('samples', file)) for file in os.listdir('samples')]
    """
    import shutil
    for file in os.listdir('samples_downstream'):
        sample = torch.load(os.path.join('samples_downstream', file))
        shutil.copy(sample.sample_path[0], 'samples_upstream/'+file)
        sample.sample_path[0] = file
        torch.save(sample, 'samples/'+file)

    sys.exit()
    """
    if 'model' in models:
        models['gnn'].eval()

    print("MODEL KEY LIST:", model_key_list)
    with torch.inference_mode():
        run_subset(args=args, data_list=data_list, device=device, models=models, model_key_list=model_key_list)

if __name__ == "__main__":
    main()
